import http from "../http-common";

class SchemasDataService {
    getAll() {
        return http.get("/schema/");
    }

    getSchema(schema) {
        return http.get(`/schema/${schema}/`);
    }
}

export default new SchemasDataService();
