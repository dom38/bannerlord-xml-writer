import React, { Component } from "react";
import { Switch, Route, Router} from "react-router-dom";
import "./App.css";
import "./css/sidebar.css"
import { styles } from "./css-common"

import ReturnForm from './components/form.component'
import ButtonAppBar from './components/appBar.component'
import DisplayOneResult from './components/returnOne.component'
import DisplayAllResults from './components/returnAll.component'
import ReturnEditForm from './components/editForm.component'
import Callback from './components/callback.component'
import HomePage from './components/home.component'
import UploadPage from './components/upload.component'

import { withStyles } from '@material-ui/core';

import { createBrowserHistory } from 'history'

const history = createBrowserHistory()

class App extends Component {
  render() {

    return (
      <div className="App" id="outer-container">

        <ButtonAppBar />
        <Router history= {history}>
          <Route exact path="/callback" component={Callback}/>
        </Router>
          <Switch>
            <Route className="returnForm" path="/:entity/add" component={ReturnForm}/>
            <Route path="/:entity/:id/edit/" component={ReturnEditForm}/>
            <Route className="upload" path="/upload/" component={UploadPage}/>
            <Route className="returnForm" path="/:entity/:id" component={DisplayOneResult}/>
            <Route className="returnForm" path="/:entity" component={DisplayAllResults}/>
            <Route classname="home" path="/" component={HomePage}/>
          </Switch>
      </div>
    );
  }
}

export default withStyles(styles)(App);
