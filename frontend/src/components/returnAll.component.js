import React, { useEffect, useState } from 'react';
import EntitiesDataService from '../services/entities.service'
import { useHistory } from "react-router"
import { useParams, Link, Redirect } from 'react-router-dom';
import MUIDataTable from "mui-datatables";
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import clsx from 'clsx';
import FileSaver from 'file-saver';
import { makeStyles } from '@material-ui/core/styles';
import { isAuthenticated } from '../lib/authUtils';
import { GetColumnNames } from '../lib/GetColumnNames'
import appConfig from '../config/app-config.json';

const useStyles = makeStyles(theme => ({
    addButton: {
      flexGrow: 1,
      marginBottom: '10px',
      position: 'absolute',
      left: '5%',
    },
    exportButton: {
      flexGrow: 1,
      marginBottom: '10px',
      position: 'absolute',
      right: '5%',
    },
  }));

export default function DisplayAllResults() {

  let history = useHistory()
  const classes = useStyles();
  const [appState, setAppState] = useState({
      loading: false,
      result: null,
  });
  
  let {entity} = useParams()

  useEffect(() => {
      setAppState({ loading: true });
      EntitiesDataService.getAll(entity).then((result) => {
        console.log(result.data)
        setAppState({ 
          loading: true, 
          result: result.data
        });
      });
    }, [setAppState, entity]);

    if(appState.result && appState.result.length > 0) {
      var columns = GetColumnNames(appState.result, entity)
      columns = new Set(columns)
      columns.delete('._id')
      columns = Array.from(columns)
      console.log(columns)
    }

    if(!appState.loading) return (<div>Loading</div>);
    if(!appState.result) return (<div>Loading</div>);

    function generateAddButton() {

      const addButton = 
          <Button
              component= {Link}
              variant="contained"
              to={`/${entity}/add`}
              color="primary"
              className={classes.addButton}
          >
              <AddIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
              Add New {entity}
          </Button>
      return addButton
    }

  function generateExportButton() {

    const addButton = 
        <Button
        // TODO: hardcoded URL, need to extract to central config
            onClick= {() => {FileSaver.saveAs(`${appConfig.apiUri}/api/entities/${entity}/export/`, `${entity}.xml`)}}
            download={`${entity}.xml`}
            variant="contained"
            color="primary"
            className={classes.exportButton}
        >
            <ImportExportIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
            Export
        </Button>
    return addButton
  }

    const options = {
      filter: true,
      download: false,
      filterType: 'dropdown',
      responsive: 'vertical',
      enableNestedDataAccess: '.',
      onRowClick: rowData =>  history.push(`/${entity}/${rowData[0]}`),
    };

    if (!isAuthenticated()) {
      return <Redirect to="/" />;
    }
    
    return (
        <div>
            {generateAddButton()}
            {generateExportButton()}
          <div class='resultTable'>
              <MUIDataTable
                  title={entity}
                  data={appState.result}
                  columns={columns}
                  options={options}
                  />

          </div>
        </div>

    
      
    )

}
