import SchemaDataService from '../services/schemas.service'
import React, { Component } from "react";
import Form from "@rjsf/material-ui";
import EntitiesDataService from '../services/entities.service';
import ErrorPage from './errorPage.component'
import Button from '@material-ui/core/Button';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { isAuthenticated } from '../lib/authUtils';
import { Redirect } from 'react-router-dom';

export default class ReturnForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
          schema: {},
          schemaType: '',
          returnValue: null,
          isSubmitted: false,
          error: false,
          errorMessage: '',
          errorInformation: '',
      };

    this.handleSubmit = this.handleSubmit.bind(this);
  }


      componentDidMount() {
        
        const schemaType = this.props.match.params.entity
        console.log(schemaType)
        SchemaDataService.getSchema(schemaType)
        .then(response => {
          console.log(response)
            this.setState({
              schema: response.data
            }, () => console.log(response.data) );
            console.log(response.data);
          })
          .catch(e => {
            console.log(e);
          });
    }

    handleSubmit(event) {
      EntitiesDataService.create(this.props.match.params.entity, event.formData)
        .then((response) => {
          console.log(response)
          this.setState({
            returnValue: response.data,
            isSubmitted: true
          });
          console.log(this.state.returnValue)
        }, (error) => {
          console.log("Error")
          console.log(error.response);
          this.setState({
            error: true,
            errorMessage: error.response.data.message,
            errorInformation: error.response.data.validationError,
          })
        });
    }

    refreshPage() {
      window.location.reload(false);
    }

    render() {
      if (!isAuthenticated()) {
        return <Redirect to="/" />;
      }
      if (this.state.error && this.state.errorMessage && this.state.errorInformation) {
        return (
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={this.refreshPage}
          >
              <ArrowBackIosIcon />
              Back
          </Button>
          
        <ErrorPage message = {this.state.errorMessage} validationError = {this.state.errorInformation}/>
         </div> )
      } else if (!this.state.error) {
        return( 
          <div class="formDiv">
            <Form 
                schema = { this.state.schema } 
                onSubmit={ this.handleSubmit }
                className="form"
              />
          </div>
                
        )
      } else {
        return null
      }
        
        
    }

}
