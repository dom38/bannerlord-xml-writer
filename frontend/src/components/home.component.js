import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { isAuthenticated } from '../lib/authUtils';
import Cookies from 'js-cookie'
import { Accordion, Grid } from '@material-ui/core';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

const useStyles = makeStyles(theme => ({

    initial: {
        flexGrow: 1,
        paddingTop: '5%',
    },
    
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.primary,
    },

    heading: {
        
    },
    
}));

export default function HomePage() {
    const classes = useStyles();

    function returnWelcomeText() {
        if (isAuthenticated()) {
            return (
                <Paper className={classes.paper} elevation={3}>
                    Welcome {Cookies.get('username')}! Use the Menu to get started.
                </Paper>
            )
        } else {
            return (
                <Paper className={classes.paper} elevation={3}>
                    Please Sign Up/Login before accessing any resources.
                </Paper>
            )
        }
    }

    function helperBox() {
        return (
            <Accordion elevation={3}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography className={classes.heading} elevation={3}>How do I even use this tool?</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        A slim tutorial on how to use the tool. Need to get Dividers, may use a modal pop-up instead of an accordian?
                    </Typography>
                </AccordionDetails>
            </Accordion>
            
        )
    } 

    function discordEmbed() {
        return (
            <Paper className={classes.paper} elevation={3}>
                        <iframe title='discordWidget' src="https://discord.com/widget?id=711589291889000499&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
                    </Paper>
        )
    }

    function carousel() {
        return (
            <Carousel>
                <div>
                    <iframe title='image1' src="https://drive.google.com/file/d/1vjSzLquH3ycDexdW2tce8iZe3DBzj6K7/preview" width="640" height="480"></iframe>
                </div>
                <div>
                    <iframe title='image2' src="https://drive.google.com/file/d/18bK7eKHsgEEHxyOzpTX3HAA7FdKPHLuH/preview" width="640" height="480"></iframe>
                </div>
                <div>
                    <iframe title='image3' src="https://drive.google.com/file/d/1mKPsKmGRuzCbradwOytPZllBGfUhXXjU/preview" width="640" height="480"></iframe>
                </div>
                <div>
                    <iframe title='image4' src="https://drive.google.com/file/d/1CyXBnSwz9vPJgpGulPqflK7t3EDzirOW/preview" width="640" height="480"></iframe>
                </div>
                <div>
                    <iframe title='image5' src="https://drive.google.com/file/d/1pjzuNp6u8qB8EmqfHHzbaPHMt3By13_w/preview" width="640" height="480"></iframe>
                </div>
                <div>
                    <iframe title='image6' src="https://drive.google.com/file/d/1fzIunkmsOKkPm4XijnvIAWSfUywIwuGp/preview" width="640" height="480"></iframe>
                </div>
            </Carousel>
        )
    }

    return (
        <Container className={classes.initial}>

            <Grid container spacing={3}>
                <Grid item xs>
                    {returnWelcomeText()}
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs>
                    {helperBox()}
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs>
                    <Paper className={classes.paper}>
                        <a href="https://drive.google.com/drive/folders/0Bw8PQHRQWG8RLTRORzNBVGVPdU0">
                            GDrive
                        </a>
                    </Paper>
                </Grid>
                <Grid item xs>
                    <Paper className={classes.paper}>
                        <a href="https://www.youtube.com/channel/UCJrKeknoMBS-k0nNpgu5VMQ/featured?view_as=subscriber">
                                Youtube
                        </a>
                    </Paper>
                </Grid>
                <Grid item xs>
                    <Paper className={classes.paper}>
                        <a href="https://shokuho.proboards.com/">
                            Proboard
                        </a>
                    </Paper> 
                </Grid>
            </Grid>

            <Grid container spacing={3}>
                <Grid item xs>
                    <Paper className={classes.paper}>
                        <a href="https://trello.com/b/LaKAl6E1/features">
                            Suggestions? Trello
                        </a>
                    </Paper>
                </Grid>
                <Grid item xs>
                    {discordEmbed()}
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs>
                    <Paper className={classes.paper}>{carousel()}</Paper>
                </Grid>
            </Grid>

        </Container>
    )
}
