import axios from "axios";
import Cookies from 'js-cookie';
import appConfig from './config/app-config.json';

export default axios.create({
  baseURL: `${appConfig.apiUri}/api`,
  headers: {
    'Content-Type': 'application/json',
    'access_token': Cookies.get('access_token'),
    "id_token": Cookies.get('id_token'),
  }
});
