export function GetColumnNames(object, entity) {
    var walked = []
    var stack = [{obj: object, stack: ''}]
    var result = ['_id']
    while(stack.length > 0) {
        var item = stack.pop()
        var obj = item.obj
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] == "object") {
                    var alreadyFound = false
                    for(var i = 0; i < walked.length; i++) {
                        if (walked[i] === obj[property]) {
                            alreadyFound = true
                            break
                        }
                    }
                    if (!alreadyFound) {
                        walked.push(obj[property])
                        stack.push({obj: obj[property], stack: item.stack + '.' + property})
                    }
                }
                else {
                    if (!property.includes("_id")) {
                        result.push(item.stack.substring(item.stack.indexOf(entity)) + '.' + property)
                    }
                    
                }
            }
        }
    }

    return result
}
