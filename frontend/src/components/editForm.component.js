import SchemaDataService from '../services/schemas.service'
import React, { Component } from "react";
import Form from "@rjsf/material-ui";
import EntitiesDataService from '../services/entities.service';
import Button from '@material-ui/core/Button';
import { Link, Redirect } from 'react-router-dom';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { isAuthenticated } from '../lib/authUtils';

class ReturnEditForm extends Component {

    constructor(props) {
      super(props);
      this.state = {
            schema: {},
            schemaType: '',
            returnValue: null,
            isSubmitted: false,
            formData: null
        };
  
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
  
        componentDidMount() {
          
          const schemaType = this.props.match.params.entity
          console.log(schemaType)
          SchemaDataService.getSchema(schemaType)
          .then(response => {
            console.log(response)
              this.setState({
                schema: response.data
              }, () => console.log(response.data) );
              console.log(response.data);
            })
            .catch(e => {
              console.log(e);
            });
            EntitiesDataService.get(schemaType, this.props.match.params.id)
            .then(response => {
                let formDataParsed = response.data
                console.log(response)
                  this.setState({
                    formData: formDataParsed
                  }, () => console.log(response.data) );
                  console.log(this.state.formData);
                })
                .catch(e => {
                  console.log(e);
                });
      }
  
      handleSubmit(event) {
        EntitiesDataService.update(this.props.match.params.entity, this.props.match.params.id, event.formData)
          .then((response) => {console.log(response)
            .this.setState({
              returnValue: response.data,
              isSubmitted: true
            })
            console.log(this.state.returnValue)
          }, (error) => {
            console.log(error)
          })
      }

      back () {
        // const { classes } = this.props
        const editButton = 
          <Button
              component = {Link}
              variant="contained"
              color="primary"
              to={`/${this.props.match.params.entity}/${this.props.match.params.id}`}
          >
              <ArrowBackIosIcon />
              Edit
          </Button>
      return editButton
    }
  
      render() {
        if (!isAuthenticated()) {
          return <Redirect to="/" />;
        }
        if (!this.state.formData) {
            return <div>Loading</div>
        }
          return( 

            <div>
              <div class="backButton">
              <Button
              component = {Link}
              variant="contained"
              color="primary"
              to={`/${this.props.match.params.entity}/${this.props.match.params.id}`}
          >
              <ArrowBackIosIcon />
              Back
          </Button> 
          </div>
            <div class="formDiv">
              <Form 
                  schema = { this.state.schema } 
                  onSubmit = { this.handleSubmit }
                  formData = { this.state.formData }/>
            </div>
            </div>
                  
          )
          
      }
  
  }

  export default ReturnEditForm
