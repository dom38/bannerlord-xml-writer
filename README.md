# Bannerlord XML Writer

## What is it?

Bannerlord uses files called XMLs to hold data about settlements, Items, Dialog, and more. These files ti together all the assets in the game. XMLs are human-readable but not very user friendly, and very easy to break. Loading a broken XML into Bannerlord via a mod can lead to issues that are incredibly difficult to resolve.

This project aims to have an intuitive front-end that allows users, no matter their technical expertise, to write XMLs that will do exactly what they want. It is designed to run on a web server and be accessible over the internet, but can be run locally with a few commands.

## How do I help test the application?

Since the XMLs mentioned above are complicated, I've not been able to test every single permutation to check that the application is making the right XMLs. The application itself does a chack on the XML before it is saved and rejects it if it is incorrect, so by filling in the forms and checking that the data is saved, anyone can test, feedback to myself and I can change the front-end application to render the correct XMLs to pass to the database.


## How do I run the front-end application locally?

1. Install [node js](https://nodejs.org/en/) 10+
2. Clone this repo
3. In the frontend folder, run `npm install`
4. In the frontend folder, run `npm start`, then navigate to `http://localhost:3000/`

This is the web app, there is an API that the web app contacts hosted on `https://serverless.shokuho.net/api/`. If you want to host the API yourself ([found here](https://gitlab.com/dom38/bannerlord-xml-writer-api/-/tree/master)) you need to update `frontend/src/config/app-config.json` with the location of your API run (Probably port 8000) and any axios calls need to not send custom headers which is configured in `frontend/src/http-common.js` (Just comment out the access_token and id_token headers, I'll put this in config at some point).


## The application is running, now what?

1. Got to the menu in the top left corner and click to see all the types of XMLs supported:

![menu](images/menu.PNG)

2. Click on any to be taken to a view where they can be looked at, added, edited and exported:

![entity](images/entity.PNG)

3. Click "Add New \{Whatever\}" button to be taken to a form to input a new entity:

![form](images/form.PNG)

4. Forms sometimes have buttons where you can add things to them by clicking "Add Item":

![fullForm](images/fullForm.PNG)

The testing part is going through these forms, adding a bunch of stuff and submitting them, and seeing if it works by going back to the menu and seeing if the item you added appears there. If not, the problem is posted in the Git Bash/Terminal window. I'd like to see these problems so I can replicate them and fix them. Giving me the form you were trying to send, and the data you sent would be super helpful. When you make the request you can have the network debugger open on your browser and copy the "Request Payload" from the Request section.
