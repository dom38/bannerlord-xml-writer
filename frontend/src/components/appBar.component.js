import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from '@material-ui/core/Menu';
import SchemasDataService from '../services/schemas.service';
import ShokuhoLogo from './logo.component'
import cognitoUtils from '../lib/cognitoUtils'
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../lib/authUtils';
import Cookies from 'js-cookie'

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
      marginLeft: theme.spacing(2),
      color: 'inherit',
      textDecoration: 'none',
    },
    title: {
      flexGrow: 1,
      marginBottom: '10px',
      color: 'inherit',
      textDecoration: 'none',
    }
  }));

export default function ButtonAppBar() {
    
  const classes = useStyles();
  const [schema, setSchema] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const fetchSchema = async () => {
      const fetchedSchemas = await SchemasDataService.getAll()
      setSchema(fetchedSchemas.data)
  }

  function generateMenu() {
      const navItems = schema.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
    }).map((item) =>
      <p key={item}><a className={classes.menuButton} href={`/${item}`} key={item}>
    {item}
  </a></p>
      );
      return navItems
  }

  useEffect(() => {
    fetchSchema();
  }, []);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function returnMenu() {
    if (isAuthenticated()) {
      return (
        <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MenuIcon />
      </IconButton>
      )
    } else {
      return null
    }
  }

  function returnLoginButton() {
    if (isAuthenticated()) {
      return (
      <Button
        color="inherit" 
        component= {Link} 
        to={`/users/${Cookies.get('username')}`}  >
          {Cookies.get('username')}
        </Button>
      )
    } else {
      return (
        <Button 
          color="inherit" 
          href={cognitoUtils.getCognitoSignInUri()} >
            Login
          </Button>
        )
    }
  }

  function returnUploadButton() {
    if (isAuthenticated()) {
      return (
      <Button
        color="inherit" 
        component= {Link} 
        to={`/upload/`}  >
          Upload XML
        </Button>
      )
    } else {
      return null
    }
  }
  
  return (
    <div>
        <AppBar position="static">
      <Toolbar>
        {returnMenu()}
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {generateMenu()}
      </Menu>
        <Typography 
        display = 'initial' 
        component= {Link} 
        to='/' 
        variant="h6" 
        className={classes.title}
        >
          Shokuho
          <ShokuhoLogo className={classes.logo}/>
        </Typography>
        {returnUploadButton()}
        {returnLoginButton()}
      </Toolbar>
    </AppBar>
    </div>
  );
}
