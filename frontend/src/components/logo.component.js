import { withStyles} from '@material-ui/core/styles';
import { Icon } from "@material-ui/core";
import YourLogo from './logo_samurai_2.svg'

const styles = theme => ({
  icon: {    
    marginLeft: '10px',
    marginTop: '10px',
  },
});

function ShokuhoLogo(props) {
  const { classes } = props;

  return (      
    <Icon className={classes.icon}>
    <img src={YourLogo} height={50} width={50} alt="Big bad sammy boi"/>
</Icon>
  );
}

export default withStyles(styles)(ShokuhoLogo);
