import Axios from 'axios';
import React, {
  useMemo,
  useCallback,
  useState
} from 'react'
import {
  useDropzone
} from 'react-dropzone'
import Cookies from 'js-cookie';
import appConfig from '../config/app-config.json';
import ErrorPage from './errorPage.component';
import Button from '@material-ui/core/Button';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '50px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  marginTop: '20px',
  marginLeft: '10%',
  marginRight: '10%'
};

const activeStyle = {
  borderColor: '#2196f3'
};

const acceptStyle = {
  borderColor: '#00e676'
};

const rejectStyle = {
  borderColor: '#ff1744'
};

function MyDropzone() {

  const [errorState, setErrorState] = useState({});

  const [fileState, setFileState] = useState({
    fileList: [],
    processed: false,
    message: "False",
  })

  const onDrop = useCallback(acceptedFiles => {
    var formData = new FormData()
    var fileArray = []
    acceptedFiles.forEach((file) => {
      formData.append(file.name, file)
      setFileState({
        fileList: [...fileState.fileList, file.name]
      })
      console.log(file)
      console.log(fileState.fileList)
      fileArray.push(file.name)
      console.log(fileArray.toString())
    })
    Axios.post(`${appConfig.apiUri}/api/entities/upload/`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'access_token': Cookies.get('access_token'),
        "id_token": Cookies.get('id_token'),
      }
    }).then((response) => {
      setFileState({
        processed: true,
        message: "True",
        fileList: fileArray
      })
    }, (error) => {
      console.log(error.response)
      setFileState({
        message: "Error"
      })
      setErrorState({
        error: true,
        errorMessage: error.response.data.message,
        errorInformation: error.response.data.validationError,
      })
    })
  }, [fileState, setFileState, setErrorState])

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({
    accept: 'text/xml',
    onDrop
  });

  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isDragActive,
    isDragReject,
    isDragAccept
  ]);

  console.log(errorState.error)
  console.log(errorState.errorInformation)
  console.log(errorState.errorMessage)

  if (errorState.error && errorState.errorInformation && errorState.errorMessage) {
    return ( <div >
      <Button variant = "contained"
      color = "primary"
      onClick = {() => { window.location.reload(false) }} >
      <ArrowBackIosIcon/>
      Back </Button>

      <ErrorPage type = {
        "upload"
      }
      message = {
        errorState.errorMessage
      }
      validationError = {
        errorState.errorInformation
      }/> </div> )
    }
    else {
      return ( <div className = "container" >
        <div {
          ...getRootProps({
            style
          })
        } >
        <input {
          ...getInputProps()
        }
        /> {
          isDragActive && isDragAccept ?
            <p > Drop the files here... </p> : 
            <p > Drag 'n' drop some XML files here, or click to select files </p>
        } </div>
        <div>
              <div>
                <p>Processed: {fileState.message}</p>
                {
                  fileState.fileList ?
                    fileState.fileList.toString():
                    null
                }
              </div>
        </div>
        </div>
      )
    }
  }

  export default function UploadPage() {

    return (MyDropzone())

  }
