import http from "../http-common";

class EntitiesDataService {
  getAll(entities) {
    return http.get(`/entities/${entities}/`);
  }

  get(entities, id) {
    return http.get(`/entities/${entities}/${id}`);
  }

  create(entities, data) {
    return http.post(`/entities/${entities}/`, data);
  }

  update(entities, id, data) {
    return http.put(`/entities/${entities}/${id}/`, data);
  }

  delete(entities, id) {
    return http.delete(`/entities/${entities}/${id}/`);
  }

  findByName(entities, title) {
    return http.get(`/entities/${entities}/search/${title}/`);
  }

  getExport(entities) {
    return http.get(`/entities/${entities}/export/`);
  }
}

export default new EntitiesDataService();
