import React, { useEffect, useState } from 'react';
import EntitiesDataService from '../services/entities.service'
import { Link, useParams, Redirect } from 'react-router-dom';
import { useHistory } from "react-router"
import MUIDataTable from "mui-datatables";
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { GetColumnNames } from '../lib/GetColumnNames'
import { isAuthenticated } from '../lib/authUtils';

const useStyles = makeStyles(theme => ({
  editButton: {
    flexGrow: 1,
    marginBottom: '10px',
    position: 'absolute',
    right: '5%',
    marginRight: '5%',
  },
  backButton: {
    flexGrow: 1,
    marginBottom: '10px',
    position: 'absolute',
    left: '5%',
    marginLeft: '7.5%',
  },
  deleteButton: {
    position: 'absolute',
    left: '50%',
    right: '50%',
    marginTop: '5%',
  },
  divButton: {
    left: '5%',
    marginTop: '3%',
    marginBottom: '3%',
    position: 'relative'
  },
  divTable: {
    marginTop: '6%',
    marginLeft: '5%',
    paddingTop: '5%',
    marginRight: '5%', 
  },
}));


export default function DisplayOneResult() {

    let history = useHistory()
    var flatten = require('flat')
    const classes = useStyles();
    const [appState, setAppState] = useState({
        loading: false,
        result: null,
    });
    
    let {entity} = useParams()
    let {id} = useParams()

    useEffect(() => {
        setAppState({ loading: true });
        EntitiesDataService.get(entity, id).then((result) => {
          const returnedEntity = result.data;
          setAppState({ loading: true, result: [returnedEntity] });
        });
      }, [setAppState, entity, id, flatten]);

      if(appState.result && appState.result.length > 0) {
        var columns = GetColumnNames(appState.result, entity)
        columns = new Set(columns)
        columns.delete('._id')
        columns = Array.from(columns)
      }
      console.log(columns)
      console.log("appstate "+JSON.stringify(appState.result))
      if(!appState.loading) return (<div>Loading</div>);
      if(!appState.result) {
          return (<div>Loading</div>)
      }

      function deleteOnClick () {
        EntitiesDataService.delete(entity, id).then((result) => {
          console.log(result.data)
          history.push(`/${entity}`)
        });
      }

      const options = {
        filter: false,
        download: false,
        filterType: 'dropdown',
        responsive: 'vertical',
        enableNestedDataAccess: '.',
      };

      if (!isAuthenticated()) {
        return <Redirect to="/" />;
      }

      return (
        <div>
            <div className={classes.divButton}>
              <Button
                component = {Link}
                variant="contained"
                color="primary"
                to={`/${entity}/${id}/edit`}
                className={classes.editButton}
            >
                <EditIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                Edit
            </Button>
            </div>

            <div className={classes.backButton}>
              <Button
                component = {Link}
                variant="contained"
                color="primary"
                to={`/${entity}`}
                className={classes.editButton}
            >
                <ArrowBackIosIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                Back
            </Button>
            </div>

            <div class='resultTable'>
              <MUIDataTable
                title={entity}
                data={appState.result}
                columns={columns}
                options={options}
              /> 
          </div>

          <div className={classes.deleteButton}>
              <Button
                onClick={deleteOnClick}
                variant="contained"
                color="secondary"
                className={classes.editButton}
            >
                <DeleteForeverIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
                Delete
            </Button>
            </div>
        </div>
      )

}
