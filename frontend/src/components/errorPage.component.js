import { makeStyles } from '@material-ui/core/styles';
import { CopyBlock, dracula } from "react-code-blocks";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({

    skulls: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    container: {
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    title: {

    },
    body: {

    }
    
}));

export default function ErrorPage(props) {

    const classes = useStyles();

    function issueReturn(code) {
        return (
          <CopyBlock
            codeBlock='true'
            text={code}
            theme={dracula}
          />
        );
    }

    function ContainerReturn () {

        if (props.type === "upload") {
            return (
                <Container className={classes.container}>
                    <img src='https://i.ytimg.com/vi/lYK66wXW8kE/hqdefault.jpg' alt='I will drink from your skull!' className={classes.skulls}/>
    
                    <Typography variant="h2" className={classes.title}>
                        Bad XML
                    </Typography>
                    <Typography variant="h6" className={classes.body}>
                        Your XML didn't pass validation against the schema and was rejected by the server
                    </Typography>
                    <Typography variant="h6" className={classes.body}>
                        You can find schemas to validate your XML against in the Bannerlord directory, or here: https://gitlab.com/dom38/bannerlord-xml-writer-api/-/tree/master/schema/schemas 
                    </Typography>
    
                    <Typography variant="h6" className={classes.body}>
                        Here is the validation error the server raised:
    
                    </Typography>
    
    
                    {issueReturn(props.validationError)}
    
                </Container>
            )
        }
        return (
            <Container className={classes.container}>
                <img src='https://i.kym-cdn.com/photos/images/newsfeed/001/350/668/6e1.jpg' alt='I will drink from your skull!' className={classes.skulls}/>

                <Typography variant="h2" className={classes.title}>
                    Big ol goofy
                </Typography>
                <Typography variant="h6" className={classes.body}>
                    Please go to https://gitlab.com/dom38/bannerlord-xml-writer/-/issues and create a new issue.
                </Typography>
                <Typography variant="h6" className={classes.body}>
                    Its title should be: "{props.message}" (Please add a number if there's already an issue with this title)
                </Typography>

                <Typography variant="h6" className={classes.body}>
                    And please copy this into the body:

                </Typography>


                {issueReturn(props.validationError)}

            </Container>
        )
    }

    return (
        <ContainerReturn />
    )

}
